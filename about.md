---
layout: page
title: About
permalink: /about/
---

This is a blog created by a citizen of Republic of India. I believe that India is a republic — the supreme power of this country lies in the body of citizens. The Prime Minister is not a monarch (king or queen), but an elected representative of the citizens. His/Her reponsibility is to represent the voice of people.

But this idea/system has been polluted in the past and continues to be polluted still. Politicians today think of themselves as powerful dictators. Today, a person is qualified to oppress or order other people just because they know someone in the political party in power. Mob lynches one for not "obeying" beliefs setup by the ruling party. When someone tries to express the fear of living in a country like this, they are asked to leave this country instead of being heard. Nobody feels a responsibility for making this country better to live.

This blog is an effort of an unnamed citizen to put forward his/her thoughts without being concerned about dangers of speaking up with an identity that is known to other people. I, a citizen, can be anyone living in this country who wants to speak up or express their worldview without being asked to leave and without receiving death threats.

If you would like to publish your opinions into this blog you can email your write-ups to **eknaagrik@tutanota.com**.

_I recommend using an encrypted email service for communicating with me, as your ideas about what India should be might not be welcome to the people in power._

## Guidelines for articles:
- Articles have to be secular in every sense of the word (releigion, caste, geography, color). This does not mean that one cannot be critical about society, rather that is encouraged. But saying something about a whole community based on your own personal experience is over-generalisation.
- Articles should not be biased (it can be critical).
- Any facts and figures mentioned should have their sources linked.
- Articles should be written in proper English grammar (the editor can help you with that).
- Please email them from an email id @tutanota.com. Tutanota is an end-to-end encrypted email service, which will help you stay anonymous. So if a reader doesn't like your opinions, you will stay anonymous.

**Disclaimer:**
This blog doesn't support any political, religious and/or communal establishment, entity or instituition. Any resemblence of views/ideas with any kind of individuals and/or groups is purely a coincidence.