---
layout: post
title: NaMo is a personality cult
date: 2019-03-18 18:30:00 +0000
categories: []
published: true
tags:
- '2019'
- Election
- Politics
- India
- Modi
- NaMo
- Narendra Modi

---

<blockquote class="twitter-tweet"><p lang="und" dir="ltr"><a href="https://twitter.com/hashtag/WeWantChowkidar?src=hash&amp;ref_src=twsrc%5Etfw">#WeWantChowkidar</a> <a href="https://twitter.com/hashtag/ChowkidarNarendraModi?src=hash&amp;ref_src=twsrc%5Etfw">#ChowkidarNarendraModi</a> <a href="https://twitter.com/hashtag/ChowkidarPhirSe?src=hash&amp;ref_src=twsrc%5Etfw">#ChowkidarPhirSe</a> <a href="https://t.co/YwhvVQzteD">pic.twitter.com/YwhvVQzteD</a></p>&mdash; YOGESH NEHRA (@YOGESHNEHRA268) <a href="https://twitter.com/YOGESHNEHRA268/status/1107967741129052160?ref_src=twsrc%5Etfw">March 19, 2019</a></blockquote> 

Idealizing a person for anyone hasn't worked perfectly ever. It becomes hard to accept that the person one thought to be ideal isn't an ideal one even after it is evident. For instance, consider Ram Rahim Singh, who was the face of a religious cult in Sirsa, Haryana. The CBI convicted him of raping girls (yes, plural) after a long multi-year trial. His followers stooped so low that they harmed public property and people's private properties in Panchkula. The crux of the religious teachings was forgotten because they thought "the courts were against Ram Rahim". Not for a second did they think that Ram Rahim was conning them since whenever they were following him. Similar was the case with Asa Ram Bapu. And many people might be surprised to know that even idealizing Martin Luther King Jr. (MLK) can be disappointing since he was a womanizer and didn't have much respect for them. All his talk of equality, suddenly sounds hollow after that and it seems that he was just fighting for himself.

Similar is the case with the current prime servant (PS) of India Narendra Modi (official title: Prime Minister (PM); it has been replaced with prime servant on the basis of words said by PM himself after getting elected). There is a certain narrative in the country that is being normalised. To a great extent, it has been normalised.

### Favourable conditions

> There is no alternative (TINA) to Narendra Modi.

The previous PM was not a great leader, in the sense that great leaders make people believe in the ideas they themselves believe in like MLK. He was and is an incredible economist, it is probably his efforts since before 1991 that India is one of the top 10 economies of the world today. But he couldn't make people believe in his potential, and became to be known as a silent PM.

Secondly, the corruption scams uncovered in his tenure were of the scale never imagined before. The then-PM didn't take any harsh actions towards them, neither did the judiciary. Most people allegedly involved in those scams are still free to conduct business in this country while the court cases are pending. Not only that, they are even free to be election candidates. The ones who make our laws are also the ones exempted from it.

Thirdly, as a reaction to the scams, an anti-corrpution movement hit the country asking for a Lokpal. There were hunger-strikes, media coverage, huge gatherings, celebrity involvement and so on and so forth. This all led to an anti-incumbency sentiment floating across the country.

Finally, the funding of political parties was not under the Right to Information Act. This led to BJP securing an absurd amount of funding. Probably more than the scams under the previous government. Consequently, the BJP had an opportunity to setup an IT cell and even buy out some of the media outlets and run prime time TV shows pushing the narrative of _TINA_.

### Creating the Modi brand

> Abki baar Modi sarkaar

When BJP announced their PM candidate, the candidate became the party. All the advertisement revolved around Narendra Modi. The party was mentioned scarcely during the whole campaigning. The BJP spokespersons were saying "We're here for Modi ji". Modi's agenda was prime. 

The story of Modi was told as someone who sold _chai_ and someone who lived in poverty and so on and so forth. This narrative was told by all BJP people and further amplified by the media. While Rahul Gandhi, who wasn't even a PM candidate officially, was declared a _shehzaada_ (a prince). The whole competition which should have been focused on the party's manifestos was toppled. And we all love a rags-to-riches story against a villain who has a lot of power.

All I have to say now is: India is a democracy. We gave an opportunity to Modi to run this country for 5 years. It is true that GST, probably, is a good thing which I would attribute to all the people working for the libralization of economy of the country since 1991. Another good thing under the current government is the ease of doing business index of India gained points. Other than that, all we got was demonetization, Rafale scam (or later: a defence ministry office that isn't secure), name-changing of public schemes, huge number of army servicemen losing their lives on the LoC, a more dangerous situation on the border, reduced social security for the minorities, unemployment and reduced human development index. A democracy is an experimental process for the country and it's residents. It is time to give a chance to someone else: Rahul Gandhi, Priyanka Gandhi, Mayawati, Akhilesh Yadav, Mamata Banarjee are all alternatives. Don't be a victim to brand blindness.