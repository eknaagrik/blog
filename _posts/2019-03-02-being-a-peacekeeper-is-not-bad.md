---
layout: post
title: Being a peacekeeper is still good
date: 2019-03-02 18:30:00 +0000
categories: []
tags:
- pulwama
- pakistan
- attack
published: true

---
My country, India, has been involved in many conflicts since its formation. According to most historical literature out there, India has always held a moral high-ground. It might have been because our ancestors struggled for freedom. People living since before 1947, had utmost value for the independence they were granted. There is an ongoing debate whether it was the struggle (or revolution – whatever words you use) that lead to the independence of India and Pakistan or it was a ruined England after the WWII which couldn't keep worrying about a huge landmass oceans across due to lack of resources. This debate is better left for historians, but more often than not, it is impossible to figure the causes of historical events. Regardless, this article is about morality that was dear to India.

India has always followed a policy of no first attack. And India has been one of the leaders of the non-alignment movement during the Cold War. The world we live in doesn't allow a completely peaceful state without a defense force. A country should be able to protect its geographical borders from the ones who want to attack her. So India, like other countries, had to keep investing and building her army. Numerous terrorist attacks have happened in India, and India has always bounced back in almost no time. Many of them have been orchestrated by the terrorist organizations based in Pakistan like 2001 attack on Parliament, 26/11 Mumbai attacks that took 3-days to contain, Mumbai local train bombs and, more recently, attacks on the Indian Army in Uri, Pathankot and Pulwama.

India has responded to these attacks with strikes on training camps of terror groups. But never has been a response so talked about in the media as the surgical strike after Uri. And never has it escalated to the point as it was after Pulwama. Indian media almost forgot to clarify that the air strike wasn't conducted on Pakistan itself, but on the terrorist camps in Pakistan occupied Kashmir (PoK). The enemy was not Pakistan, but the media failed to report that. Suddenly, all previous governments (and their PMs), who decided to keep peace and approach the problems more diplomatically, were considered cowards. Suddenly, being a peace-keeper is bad.

> The people who want war either don't know what war is, or don't understand anything else.

It is probably true that India will win a war against Pakistan again. Probably not. China is a huge power next door that we keep ignoring except for a few op-ed articles, and China is with Pakistan as signaled by the UNSC vote to declare Masood Azhar an international terrorist. I am not going to talk about the consequences of war. For all I know, a lot of bloodshed will be a part of it. People will die, army personnel will die, countries will be destroyed; it's really hard to rebuild a country as big as India and even harder to forget the loss and move on. The truth is India and Pakistan exist now, and we better learn how to live with it.

## What about the people who died for the Kashmir cause?

The idea that Kashmir is Indian is an ancient one. It is true that ideally Kashmir was acceded to India, but we don't live in an ideal world. I do not undervalue the lives of those who died in the proxy war for Kashmir, but rather I believe that they were fighting for peace and not for Kashmir itself. All of us have seen the retired army personnel, who were involved in Kashmir conflict at some point in their lives, on TV talking about going for a full-scale war. They are tired of fighting and losing lives for decades and the thought that we will be in peace after a full-scale war is what they look forward to.

Numerous lives have been lost in the war and other wars. This fact does not mean, in any possible manner, that more lives have to be lost. We should send more people to our borders to protect them from terrorists, the ones who want to make our lives worse. But each country has civilians living who haven't done anything wrong to any country. Civilians want to lead a normal life with some dreams. I want to lead a normal life. And let it be said: India doesn't have anything against Pakistani civilians, neither do Pakistan against Indian civilians. In fact, each Pakistani I meet is no different from us. Then why should we go for a full-scale war.

## An issue of pride

The Kashmir issue is an issue of pride for India and, possibly, Pakistan. Whoever wins over Kashmir will have something more to be proud of. One thing that deserves to make us feel more proud is maintaining peace. However, according to the current radical ideas going on within the country, the ability and confidence of going to war is what is dignified.

On one hand, they ask people like Naseeruddin Shah, who just expressed their concerns to go to Pakistan. On the other hand, they want Pakistan to vanish from the map. There is an irony here (in other words hippocracy) or there is a lack of sense. They believe that "anti-nationalists" belong to Pakistan. But if Pakistan vanishes from the map, it is either a part of India or all Pakistanis are dead. We underestimate Pakistan, it is a big nation by itself (ranks 5th by population). Killing all Pakistanis means killing about 212 million people, which is about 17 times that of WWII, not only immoral but also not possible without offending rest of the world. And if India invades Pakistan and succeeds (which by no means is an easy task), Pakistani civilians are also Indians.

> India has set a benchmark in peace-keeping in the past by not starting a war by itself even in the direst of times. Even when 2 countries which surround her are on the offensive. And that is something to be proud of.